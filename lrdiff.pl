#!/usr/bin/perl

# lrdiff.pl
# Joey Kelly
# 11/27/2022
# License: GPL version 2


# we have 2 files and want to know which lines are in one and not the other, etc.
#
# NOTE: the output files are sorted, which is probably not what you wanted :-/


use strict;

use feature qw(switch say);


my $left  = shift;
my $right = shift;

die "need a left file"  unless $left;
die "need a right file" unless $right;

chomp $left;
chomp $right;

# ___FIXME___ need more input sanitation

open (my $LF, '<', $left)   or die $!;
open (my $RF, '<', $right)  or die $!;

my @LEFT  = <$LF>;
my @RIGHT = <$RF>;

# we don't care about sorting, we just want the diffs
my %lefthash;
my %righthash;

foreach (@LEFT) {
  chomp;
  $lefthash{$_}++;
}

foreach (@RIGHT) {
  chomp;
  $righthash{$_}++;
}

close $LF;
close $RF;

# now to do some actual work
open (my $LDIFF,  '>', 'leftdiff.txt')    or die $!;
open (my $RDIFF,  '>', 'rightdiff.txt')   or die $!;
open (my $COMMON, '>', 'commonlines.txt') or die $!;

my %common;

foreach (sort keys %lefthash) {
  say $LDIFF $_ unless $righthash{$_};
  $common{$_}++ if $righthash{$_};
}

foreach (sort keys %righthash) {
  say $RDIFF $_ unless $lefthash{$_};
  $common{$_}++ if $lefthash{$_};
}

foreach (sort keys %common) {
  say $COMMON $_;
}

close $LDIFF;
close $RDIFF;
close $COMMON;
