#!/usr/bin/env perl
  
# checkptr.pl

# Joey Kelly
# 7/26/2021
# License: GPL version 2

# Do forward and reverse DNS lookups and report on any mismatch.
  
use strict;
use warnings;

use feature qw(switch say);
  
  

my $fqdn   = shift;
my $server = shift;

$fqdn = '--usage' unless $fqdn;       # give some hint, don't let them quess
chomp $fqdn;
$fqdn = '--usage' if $fqdn eq '-u';
$fqdn = '--usage' if $fqdn eq '-h';
$fqdn = '--usage' if $fqdn eq '--help';

# ___FIXME__ need to regex this, die/carp if not right
#$fqdn =~ s/\.$//;
$fqdn = safechars($fqdn);

$server = '' unless  $server;
chomp $server;
# ___FIXME___ regex for an IP address

if ($fqdn eq '--usage') {
  say "usage:";
  #say "  ./checkptr.pl www.somedomain.com";
  say "  ./checkptr.pl www.somedomain.com (server)";
  say "\n";
  say "  '-u, --usage:  display usage hints";
  say "  '-h, --help:   ditto";
  exit;
} else {
  my $forward = forwardlookup($fqdn);
  say "IP4: $$forward{4}";
  say "IP6: $$forward{6}";

  my $reverse4 = ''; 
  $reverse4 = reverselookup($$forward{4}) if $$forward{4};
  my $reverse6 = '';
  $reverse6 = reverselookup($$forward{6}) if $$forward{6};
  say "4space reverse = $reverse4";
  say "6space reverse = $reverse6" if $reverse6;
  say "   *** PTR DOES NOT MATCH FQDN ***" unless "$fqdn" eq $reverse4;
}

  


# ___FIXME___ need to account for multiple IP addresses, e.g. google.com
# ...need to check reverse against all returned A records, see if any match
sub forwardlookup {
  my $fqdn   = shift;
  my $server = shift;

  my $result;
  if ($server) {
    open $result, "host $fqdn $server |";
  } else {
    # we use system defaults (see /etc/resolv.conf)
    open $result, "host $fqdn |";
  }
  my @ip = <$result>;
  
  my $trash;
  my %ip;
  $ip{4} = '';
  $ip{6} = '';
  foreach (@ip) {
    chomp;
    if ($_ =~ /has address/) {
      ($trash, $ip{4}) = split 'address ' unless $ip{4};
    }
    if ($_ =~ /has IPv6 address/) {
      ($trash, $ip{6}) = split 'address ' unless $ip{6};
    }
  }

  return \%ip;
}
  
  

# subs

sub reverselookup {
  my $ip     = shift;
  my $server = shift;

  my $result;
  if ($server) {
    open $result, "host $ip $server |";
  } else {
    open $result, "host $ip |";
  }
  my $hostname = <$result>;
  chomp $hostname;
  # ___FIXME___ do we really want to return nothing? maybe the script logic dictates that, or maybe not
  return '' if $hostname =~ 'NXDOMAIN';
  
  my @hostname = split /\s+/, $hostname;
  $hostname = pop @hostname;
  chop $hostname;   # lose the trailing dot

  return $hostname;
}



sub safechars {
  my $string = shift;
  $string =~ tr/a-zA-Z0-9:_.-//dc;   # what other characters are legal for hostnames?
  return $string;
}

