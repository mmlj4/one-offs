#!/usr/bin/perl

# ssldatecheck.pl

# Joey Kelly
# 12/07/2021
# License: GPL version 2

# Get the expiration date of an SSL certificate.
# This is merely a wrapper aroung the usual SSL commands.


use strict;
use warnings;

my $host = shift;
die "need a hostname" unless defined $host;
chomp $host;

my $port = shift;
chomp $port if defined $port;
$port = 443 unless $port;


my $command = ": | openssl s_client -servername $host -connect $host:$port | openssl x509 -noout -dates";
print "running '$command'\n\n";
my $run = `$command`;
print "$run";
