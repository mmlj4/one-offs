#!/usr/bin/perl

# subgrep.pl
# Joey Kelly
# 12/22/2022
# License: GPL version 2

# this is really only useful for finding subs in a Perl script

use strict;
use warnings;

use feature qw(say);

my $file = shift;
die 'need a file to grep' unless $file;

my $term = shift;
die 'need a search term' unless $term;

my $num = shift;
$num = '-n' if $num;

my $cmd;
if ($num) {
  $cmd = "grep $num ^sub $file | grep $term | grep $term | sed 's/:sub//' | awk '{print \$1, \"\t\", \$2}'";
} else {
  $cmd = "grep $num ^sub $file | grep $term | grep $term  | awk '{print \$2}'";
}
#say     $cmd;
system  $cmd;