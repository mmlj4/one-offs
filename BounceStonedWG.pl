#!/usr/local/bin/perl

# Joey Kelly
# 2/28/2024
# License: GPL version 2

# we want to bounce a stoned wg instance when the configured hostname changes its IP,
# since wirequard never checks DNS after the instance is started

# NOTE: this is obviously written for FreeBSD

# 15 */2 * * * /root/bin/BounceStonedWG.pl  > /dev/null 2>&1

use strict;
use warnings;

use feature qw(switch say);

my $wgif      = 'wg0';
my $hostname  = 'happygolucky.example.com';

my $WG      = '/usr/bin/wg';
my $GREP    = '/usr/bin/grep';
my $HOST    = '/usr/bin/host';
my $WGQUICK = '/usr/local/bin/wg-quick';
my $SLEEP   = '/bin/sleep';

my $connected = `$WG show $wgif | $GREP endpoint`;
my @ip        = split ':', $connected;
my $currentip = $ip[1];
$currentip    =~ s/^\ +|\ +$//g;    # removes blanks fore and aft
say "current IP = $currentip";

my $check   = `$HOST $hostname`;
my @checked = split ' ', $check;
my $newdns  = $checked[3];
chomp $newdns;
say "new DNS = $newdns";

if ($currentip ne $newdns) {
  my $bounce = "$WGQUICK down $wgif ; $SLEEP 3 ; $WGQUICK up $wgif";
  say $bounce;
  system $bounce;
}
