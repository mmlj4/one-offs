#!/usr/bin/env perl

# longestline.pl
# Joey Kelly
# 7/26/2021
# License: GPL version 2

use strict;
use warnings;

use feature qw(switch say);

my $file = shift;
die "need a file" unless $file;
chomp $file;

my $verbose = shift;
$verbose    = 0 unless $verbose;

open my $fh, '<', $file or die "Could not open '$file' $!";
my $linenum;
my $length  = 0;
my $longest = 0;
while (<$fh>) {
  chomp;
  $linenum++;
  if (length $_ > $length) {
    $length   = length $_;
    $longest  = $linenum;
    say "longest line yet: $linenum, $length characters" if $verbose;
  }
}
close $fh;

say "the longest line of $file is $longest, at $length characters";
